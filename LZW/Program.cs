﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.IO;
using System.Linq;
using System.Reflection.Metadata.Ecma335;
using System.Text;
using Microsoft.VisualBasic.CompilerServices;

namespace LZW
{
    class Program
    {

        static void Main(string[] args)
        {
//            int okey = 0;
//            int fails = 0;
//            var failList = new List<string[]>();
//            for (int i = 0; i < 100000; i++)
//            {
//                Random rand = new Random();
//                string test = "";
//                for (int j = 0; j < 15; j++)
//                {
//                    test += (char)rand.Next(21, 145);
//                }
//                var lzw = new LZWCompress();
//                var compressed = lzw.Compress(test);
//                var decompressed = lzw.Decompress(compressed);
//                if (test.Equals(decompressed))
//                {
//                    okey++;
//                }
//                else
//                {
//                    fails++;
//                    failList.Add(new[] {test, decompressed});
//                }
//            }
            string a = "test is test";
            var lzw = new LZWCompress();
            var result = lzw.Compress(a);
            using (BinaryWriter writer = new BinaryWriter(File.Open("./temp.txt", FileMode.Create)))
            {
                writer.Write("123456789");
            }
            Console.WriteLine();
            if (File.Exists("./temp.txt"))
            {
                using (BinaryReader reader = new BinaryReader(File.Open("./temp.txt", FileMode.Open)))
                {
                    while (reader.PeekChar() != -1)
                    {
                       Console.WriteLine(reader.ReadString());
                    }
                }
            }
        }
    }

    public class LZWCompress
    {
        Dictionary<List<byte>, short> TABLE;
        /// <summary>
        /// Сжимает переданнуб строку
        /// </summary>
        /// <param name="input">строка для сжатия</param>
        /// <returns>массив "байт" кодированной строки</returns>
        /// <exception cref="Exception">Ошибка кодирования</exception>
        public short[] Compress(string input)
        {
            // Первичная инициализация
            TABLE = new Dictionary<List<byte>, short>(new ListComparer());
            // Заполнение известных 255 значений UTF-8
            for (short i = 0; i < 256; i++)
            {
                // Пустой лист байтов
                List<byte> empty = new List<byte>() { (byte)i };
                TABLE.Add(empty, i);
            }
            // Преобразуем полученную строку в байтовую последовательность
            byte[] inputBytes = new byte[input.Length];
            for (int i = 0; i < input.Length; i++)
            {
                inputBytes[i] = (byte) input[i];
            }
            // "Предыдущий" байт
            List<byte> prefixCode = new List<byte>();
            // Выходной поток сжатой строки
            List<short> outputBytes = new List<short>();

            
            foreach (var inputByte in inputBytes)
            {
                // Последовательность из текущего и предыдущего кодов
                var extendedCode = new List<byte>(prefixCode) { inputByte };
                // Если расширенный код уже был кодирован, то ничего делать не нужно
                if (TABLE.ContainsKey(extendedCode))
                {
                    prefixCode.Clear();
                    prefixCode.AddRange(extendedCode);
                }
                else
                {
                    // Если расширенный код не встречался, то необходимо его закодировать и внести в TABLE
                    if (TABLE.ContainsKey(prefixCode))
                    {
                        // Отправляем в выходной поток предыдущий байт
                        outputBytes.Add(TABLE[prefixCode]);
                    }
                    else
                    {
                        throw new Exception("Что-то пошло не так :(");
                    }
                    // Добавление нового "байта"
                    TABLE.Add(extendedCode, (short)TABLE.Count);
                    prefixCode.Clear();
                    // Текущий байт становится предыдущим
                    prefixCode.Add(inputByte);
                }
            }
            // Если остались недобавленный "байт", то отправим его в в выходной поток
            if (prefixCode.Count == 0) return outputBytes.ToArray();
            outputBytes.Add(TABLE[prefixCode]);
            prefixCode.Clear();
            return outputBytes.ToArray();
        }
        /// <summary>
        /// Разархивирует массив "байт" в строку UTF-8
        /// </summary>
        /// <param name="inputByteString">входной массив байт</param>
        /// <returns>Полученная строка в UTF-8</returns>
        /// <exception cref="Exception">Пустой входной поток</exception>
        public string Decompress(int[] inputByteString)
        {
            // Проверка на пустоту входного буфера байт или отстутствия в нем любых значений
            if (inputByteString == null || inputByteString.Length == 0)
            {
                throw new Exception("Проверьте входной массив байт");
            }
            // Преобразуем в удобную для использования структуру
            var inputBytes  = new List<int>(inputByteString);
            // Таблица декодирования бит
            var TABLE = new Dictionary<int, List<byte>>();
            // Заполнения первых 256 возможных значений: символы UTF-8
            for(var i = 0; i < 256; i++)
            {
                var empty = new List<byte> {(byte) i};
                TABLE.Add(i, empty);
            }
            // Инициализируем первоначальный перфикс - 1 символ входной строки
            var prefixCode = TABLE[inputBytes[0]];
            // Убираем его для возможности использовать foreach
            inputBytes.RemoveAt(0);
            // Выходной поток байтов с первым полученным символом
            var outputBytes = new List<byte>(prefixCode);
            foreach(var inputByte in inputBytes)
            {
                // Кандидаты на разархивацию
                var currentBytes = new List<byte>();
                if (TABLE.ContainsKey(inputByte))
                {
                    // Если уже содержится данный байт в таблице,
                    // то его нужно просто добавить в выходной поток
                    currentBytes.AddRange(TABLE[inputByte]);
                }
                else
                {
                    // Для повторяющихся символов: BBB
                    if (inputByte == TABLE.Count)
                    {
                        currentBytes.AddRange(new List<byte>(prefixCode) {prefixCode[0]});
                    }
                }

                if (currentBytes.Count <= 0) continue;
                // В выходной поток выводим текущие биты
                outputBytes.AddRange(currentBytes);
                // Заносим новую найденную пару в конец таблицы
                TABLE.Add(TABLE.Count, new List<byte>(prefixCode.ToArray().Concat(new[] {currentBytes.ToArray()[0]}).ToArray()));
                prefixCode = currentBytes; 
            }

            // Преобразуем в строку
            string result = string.Empty;
            foreach (var _byte in outputBytes)
            {
                result += (char) _byte;
            }

            return result; 
        }
        
    }
    /// <summary>
    /// Класс помощник для сравнения листов в TABLE
    /// </summary>
    public class ListComparer : IEqualityComparer<List<byte>>
    {
        public bool Equals(List<byte> x, List<byte> y)
        {
            if (x == null || y == null) return false;
            if (x.Count != y.Count) return false;
            return !x.Where((t, i) => t != y[i]).Any();
        }

        public int GetHashCode(List<byte> obj)
        {
            var hash = 0x0;
            foreach (var b in obj)
            {
                hash ^= b;
            }

            return hash;
        }
    }
}